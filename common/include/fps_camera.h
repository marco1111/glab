#ifndef GLAB_FPS_CAMERA_H
#define GLAB_FPS_CAMERA_H

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace glab {

class TrackballCamera;

/**
 * Abstraction to help to control a camera in a first-person shooter game style.
 */
class FPSCamera {
public:
	inline FPSCamera(const glm::vec3& position = glm::vec3(0,0,0))
		: _position(position), _pitch(0.0f), _yaw(glm::pi<float>())
	{ }

	FPSCamera(const TrackballCamera& cameraTB);

	inline void pitch(float angleRadians) { _pitch += angleRadians; }
	inline void yaw(float angleRadians) { _yaw += angleRadians; }

	inline void translate(const glm::vec3& amount) { _position += amount; }

	inline const glm::vec3& getPosition() const { return _position; }
	inline void setPosition(const glm::vec3& position) { _position = position; }

	inline glm::vec3 getDirection() const {
		return glm::vec3 ( // spherical coordinates to cartesian coordinates conversion
			glm::cos(_pitch) * glm::sin(_yaw),
			glm::sin(_pitch),
			glm::cos(_pitch) * glm::cos(_yaw)
		);
	}

	inline glm::vec3 getRight() const {
		return glm::vec3 (
			glm::sin(_yaw - glm::half_pi<float>()), 0.0f, glm::cos(_yaw - glm::half_pi<float>())
		);
	}

	inline glm::vec3 getUp() const {
		return glm::cross(getRight(), getDirection());
	}

	inline glm::mat4 getViewMatrix() const {
		const glm::vec3 f = getDirection();
		const glm::vec3 r = getRight();
		const glm::vec3 u = glm::cross(r, f); // up

		return glm::mat4(
			r.x, u.x, -f.x, 0,
			r.y, u.y, -f.y, 0,
			r.z, u.z, -f.z, 0,
			-glm::dot(r, _position), -glm::dot(u, _position), glm::dot(f, _position), 1.0f
		);
	}

private:
	glm::vec3 _position;
	float _pitch, _yaw;
};


}

#endif // GLAB_FPS_CAMERA_H