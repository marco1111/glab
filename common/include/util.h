#ifndef GLAB_UTIL_H
#define GLAB_UTIL_H

#include <string>
#include <vector>
#include <glm/glm.hpp>

namespace glab {
namespace util {

bool isDirectory(const std::string& str);

/**
 * Try to find a directory called @param dirName.
 *
 * @return TRUE on success. Then @param dirFullPath will be set.
 */
bool findDirectory(const std::string& dirName, std::string& dirFullPath);

/**
* @return Program ID.
*/
GLuint loadShadersFromFiles(const std::string& vertex_file_path, const std::string& fragment_file_path);
GLuint loadShaders(const std::string& vertexShaderCode, const std::string& fragmentShaderCode);

/**
 * @return Texture ID.
 */
GLuint loadBMP(const std::string& imagepath);

/**
 * @return Texture ID.
 */
GLuint loadDDS(const std::string& imagepath);

/**
 * @return TRUE on success.
 */
bool loadModel(const std::string& file, std::vector<unsigned short>& indices, std::vector<glm::vec3>& vertices, std::vector<glm::vec2>& uvs, std::vector<glm::vec3>& normals);

}
}

#endif // GLAB_UTIL_H