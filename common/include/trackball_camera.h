#ifndef GLAB_TRACKBALL_CAMERA_H
#define GLAB_TRACKBALL_CAMERA_H

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace glab {

class FPSCamera;

/**
 * Make a camera orbit a "virtual sphere" centered at a chosen target point.
 */
class TrackballCamera {
public:
	inline TrackballCamera(const glm::vec3& target, float radius)
		: _target(target), _radius(radius), _pitch(0.0f), _yaw(0.0f)
	{ }

	TrackballCamera(const FPSCamera& cameraFPS, const glm::vec3& target);

	inline void pitch(float angleRadians) {
		_pitch += angleRadians;
		_pitch = glm::clamp(_pitch, -glm::half_pi<float>()*0.999f, glm::half_pi<float>()*0.999f);
	}

	inline void yaw(float angleRadians) { _yaw += angleRadians; }

	inline float getPitch() const { return _pitch; }
	inline float getYaw() const { return _yaw; }

	inline void setTarget(const glm::vec3& target) { _target = target; }
	inline const glm::vec3& getTarget() const { return _target; }
	
	inline void setRadius(float radius) { _radius = radius; }
	inline float getRadius() const { return _radius; }

	inline glm::vec3 getPosition() const {
		return _target + glm::vec3( // spherical coordinates to cartesian coordinates conversion
			_radius * glm::cos(_pitch) * glm::sin(_yaw),
			-_radius * glm::sin(_pitch),
			_radius * glm::cos(_pitch) * glm::cos(_yaw)
		);
	}

	void setPosition(const glm::vec3& position);
	
	inline glm::mat4 getViewMatrix() const {
		return glm::lookAt(getPosition(), _target, glm::vec3(0, 1, 0));
	}

private:
	glm::vec3 _target;
	float _radius, _pitch, _yaw;
};


}

#endif // GLAB_TRACKBALL_CAMERA_H