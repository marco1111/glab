#ifndef GLAB_TEXT_RENDERER_H
#define GLAB_TEXT_RENDERER_H

#include <string>

namespace glab {

/**
 * It knows how to draw 2D text on the screen.
 */
class TextRenderer {
public:
	TextRenderer(const std::string& fontTextureFile);
	~TextRenderer();

	void printText2D(const std::string& text, float x, float y, float size) const;

private:
	unsigned _textureID;
	unsigned _vertexBufferID;
	unsigned _UVBufferID;
	unsigned _programID;
	unsigned _uniformID;
};

}

#endif // GLAB_TEXT_RENDERER_H