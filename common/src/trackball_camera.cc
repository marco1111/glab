#include "trackball_camera.h"
#include "fps_camera.h"

namespace glab {

TrackballCamera::TrackballCamera(const FPSCamera& cameraFPS, const glm::vec3& target)
	: _target(target)
{
	setPosition(cameraFPS.getPosition());
}

void TrackballCamera::setPosition(const glm::vec3& position) {
	_radius = glm::distance(position, _target);
	assert(_radius > 0.0f);

	const glm::vec3 relPosition = position - _target;

	_pitch = glm::asin(-relPosition.y / _radius);
	const float cosPitch = glm::cos(_pitch);
	assert(cosPitch > 0.0f);

	_yaw = glm::asin(relPosition.x / (_radius * cosPitch));

	if (relPosition.z < 0.0f) {
		_yaw = glm::half_pi<float>() + (glm::half_pi<float>() - _yaw);
	}
}

}