#include "fps_camera.h"
#include "trackball_camera.h"

namespace glab {

FPSCamera::FPSCamera(const TrackballCamera& cameraTB)
	: FPSCamera(cameraTB.getPosition())
{
	pitch(cameraTB.getPitch());
	yaw(cameraTB.getYaw());
}

}