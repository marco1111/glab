/**
 * Based on http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-11-2d-text/
 */

#include <GL/glew.h>

#include "text_renderer.h"
#include "util.h"

namespace glab {

const char* vs_source =
	"#version 330 core\n"

	// Input vertex data, different for all executions of this shader.
	"layout(location = 0) in vec2 vertexPosition_homoneneousspace;"
	"layout(location = 1) in vec2 vertexUV;"
	"out vec2 UV;" // Output data ; will be interpolated for each fragment.

	"void main() {"
	"	gl_Position = vec4(vertexPosition_homoneneousspace, 0, 1);"
	"	UV = vertexUV;"
	"}";

const char* fs_source =
	"#version 330 core\n"

	"in vec2 UV;" // Interpolated values from the vertex shaders
	"out vec4 color;" // Ouput data
	"uniform sampler2D myTextureSampler;"

	"void main() {"
	"	color = texture2D(myTextureSampler, UV);"
	"}";

TextRenderer::TextRenderer(const std::string& fontTextureFile) {
	// Initialize texture
	_textureID = util::loadDDS(fontTextureFile);

	// Initialize VBO
	glGenBuffers(1, &_vertexBufferID);
	glGenBuffers(1, &_UVBufferID);

	// Initialize Shader
	_programID = util::loadShaders(std::string(vs_source), std::string(fs_source));

	// Initialize uniforms' IDs
	_uniformID = glGetUniformLocation(_programID, "myTextureSampler");
}

TextRenderer::~TextRenderer() {
	// Delete buffers
	glDeleteBuffers(1, &_vertexBufferID);
	glDeleteBuffers(1, &_UVBufferID);

	// Delete texture
	glDeleteTextures(1, &_textureID);

	// Delete shader
	glDeleteProgram(_programID);
}

void TextRenderer::printText2D(const std::string& text, float x, float y, float size) const {
	const unsigned length = text.length();

	// Fill buffers
	std::vector<glm::vec2> vertices;
	std::vector<glm::vec2> UVs;

	for (unsigned i = 0u; i < length; ++i) {
		const glm::vec2 vertex_up_left = glm::vec2(x + i*size, y + size);
		const glm::vec2 vertex_up_right = glm::vec2(x + i*size + size, y + size);
		const glm::vec2 vertex_down_right = glm::vec2(x + i*size + size, y);
		const glm::vec2 vertex_down_left = glm::vec2(x + i*size, y);

		vertices.push_back(vertex_up_left);
		vertices.push_back(vertex_down_left);
		vertices.push_back(vertex_up_right);

		vertices.push_back(vertex_down_right);
		vertices.push_back(vertex_up_right);
		vertices.push_back(vertex_down_left);

		const char character = text[i];
		const float uv_x = (character % 16) / 16.0f;
		const float uv_y = (character / 16) / 16.0f;

		const glm::vec2 uv_up_left = glm::vec2(uv_x, uv_y);
		const glm::vec2 uv_up_right = glm::vec2(uv_x + 1.0f / 16.0f, uv_y);
		const glm::vec2 uv_down_right = glm::vec2(uv_x + 1.0f / 16.0f, (uv_y + 1.0f / 16.0f));
		const glm::vec2 uv_down_left = glm::vec2(uv_x, (uv_y + 1.0f / 16.0f));

		UVs.push_back(uv_up_left);
		UVs.push_back(uv_down_left);
		UVs.push_back(uv_up_right);

		UVs.push_back(uv_down_right);
		UVs.push_back(uv_up_right);
		UVs.push_back(uv_down_left);
	}

	glBindBuffer(GL_ARRAY_BUFFER, _vertexBufferID);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec2), &vertices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, _UVBufferID);
	glBufferData(GL_ARRAY_BUFFER, UVs.size() * sizeof(glm::vec2), &UVs[0], GL_STATIC_DRAW);

	// Bind shader
	glUseProgram(_programID);

	// Bind texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _textureID);
	// Set our "myTextureSampler" sampler to user Texture Unit 0
	glUniform1i(_uniformID, 0);

	// 1rst attribute buffer : vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, _vertexBufferID);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	// 2nd attribute buffer : UVs
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, _UVBufferID);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Draw call
	glDrawArrays(GL_TRIANGLES, 0, vertices.size());

	glDisable(GL_BLEND);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
}

}