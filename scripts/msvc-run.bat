:: Please, run "msvc-generate.bat" first.
::
:: It will set a minimalistic PATH variable and open the MSVC solution.
:: This way you can run samples inside the MSVC IDE.
::
:: Change the lines below according to your system.

PATH=%USERPROFILE%\workspace\glab\thirdparty\built\msvc\boost\lib;
PATH=%PATH%%USERPROFILE%\workspace\glab\scripts\msvc-solution\thirdparty\glew\Release;
PATH=%PATH%%USERPROFILE%\workspace\glab\scripts\msvc-solution\thirdparty\glew\Debug;

cd msvc-solution
glab.sln