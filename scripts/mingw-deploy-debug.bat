set BOOST_ROOT=;
set MINGWDIR=C:\Qt\Qt5.2.0-mingw\Tools\mingw48_32\bin
set QTDIR=C:\Qt\Qt5.2.0-mingw\5.2.0\mingw48_32
set CMAKE_PREFIX_PATH=%QTDIR%;

PATH="C:\Program Files (x86)\CMake 2.8\bin";%MINGWDIR%;%QTDIR%\bin;

mkdir mingw-build-debug
cd mingw-build-debug
cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=../mingw-deploy-debug -G"MinGW Makefiles" ../.. -DASSIMP_BUILD_TESTS=FALSE
mingw32-make install

cd ..

pause