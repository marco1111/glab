set BOOST_ROOT=;
set QTDIR=C:\Qt\Qt5.4.1\5.4\msvc2013_64_opengl
set CMAKE_PREFIX_PATH=%QTDIR%

PATH="C:\Program Files (x86)\CMake\bin";

mkdir msvc-solution
cd msvc-solution
cmake -G"Visual Studio 12" -DCMAKE_INSTALL_PREFIX=msvc-deploy ../.. -DGLFW_BUILD_DOCS=FALSE -DGLFW_BUILD_EXAMPLES=FALSE -DGLFW_BUILD_TESTS=FALSE

cd ..

pause