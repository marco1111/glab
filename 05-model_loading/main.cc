/**
 * Based on http://www.opengl-tutorial.org/beginners-tutorials/tutorial-7-model-loading/
 */

#include <stdio.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "util.h"
#include "trackball_camera.h"

GLFWwindow* window;

glab::TrackballCamera _camera(glm::vec3(0, 0, 0), 10.0f);
const float cameraRotationSpeed = 0.005f;
double mouseX, mouseY;

void mouseMoved(GLFWwindow* window, double xpos, double ypos) {
	const float xDiff = float(xpos - mouseX);
	const float yDiff = float(ypos - mouseY);
	mouseX = xpos;
	mouseY = ypos;

	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
		_camera.pitch(yDiff * -cameraRotationSpeed);
		_camera.yaw(xDiff * -cameraRotationSpeed);
	}
}

void mouseScroll(GLFWwindow* window, double xoffset, double yoffset) {
	_camera.setRadius(_camera.getRadius() + float(yoffset) * cameraRotationSpeed * -50);
}

void mousePressed(GLFWwindow* window, int button, int action, int mods) {
	if (action == GLFW_PRESS) {
		glfwGetCursorPos(window, &mouseX, &mouseY);
	}
}

int main() {
	if (!glfwInit()) {
		fprintf(stderr, "Failed to initialize GLFW\n");
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(1024, 768, "GLAB - Model Loading", NULL, NULL);

	if (window == NULL) {
		fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not OpenGL 3.3 compatible.\n");
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	glfwSetCursorPosCallback(window, mouseMoved);
	glfwSetMouseButtonCallback(window, mousePressed);
	glfwSetScrollCallback(window, mouseScroll);

	// init GLEW
	glewExperimental = true; // http://glew.sourceforge.net/basic.html

	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE); // Ensure we can capture the escape key being pressed below

	glClearColor(0.0f, 0.0f, 0.4f, 0.0f); // Dark blue background
	glEnable(GL_DEPTH_TEST); // Enable depth test	
	glDepthFunc(GL_LESS); // Accept fragment if it closer to the camera than the former one

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);

	std::string shadersDir;

	if (!glab::util::findDirectory("05-model_loading", shadersDir)) {
		fprintf(stderr, "Unable to find shaders directory!\n");
		return -1;
	}

	GLuint programID = glab::util::loadShadersFromFiles(shadersDir + "shader.vert", shadersDir + "shader.frag");

	if (!programID) {
		fprintf(stderr, "Failed to load shaders!\n");
		return -1;
	}

	// Get a handle for our "MVP" uniform
	GLuint matrixID = glGetUniformLocation(programID, "MVP");

	// Projection matrix : 45� Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	glm::mat4 projection = glm::perspective(glm::radians(45.0f), 4.0f / 3.0f, 0.1f, 100.0f);
	// Or, for an ortho camera :
	//glm::mat4 projection = glm::ortho(-10.0f,10.0f,-10.0f,10.0f,0.0f,100.0f); // In world coordinates

	// Model matrix : an identity matrix (model will be at the origin)
	glm::mat4 model = glm::mat4(1.0f);
	
	GLuint vertexArrayID;
	glGenVertexArrays(1, &vertexArrayID);
	glBindVertexArray(vertexArrayID);

	// Load the texture using any two methods
	GLuint textureID = glab::util::loadDDS(shadersDir + "uvmap.DDS");

	// Get a handle for our "myTextureSampler" uniform
	GLuint textureUniformID = glGetUniformLocation(programID, "myTextureSampler");

	// Read our .obj file
	std::vector<unsigned short> indices;
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals; // not used here

	if (!glab::util::loadModel(shadersDir + "cube.obj", indices, vertices, uvs, normals)) {
		fprintf(stderr, "Failed to load model!\n");
		return -1;
	}

	// fill "indices" as needed

	GLuint vertexbuffer;
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

	GLuint uvbuffer;
	glGenBuffers(1, &uvbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);

	// Generate a buffer for the indices as well
	GLuint elementbuffer;
	glGenBuffers(1, &elementbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);

	do {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		const glm::mat4 view = _camera.getViewMatrix();
		
		// Our ModelViewProjection : multiplication of our 3 matrices
		const glm::mat4 MVP = projection * view * model; // Remember, matrix multiplication is the other way around

		glUseProgram(programID);

		// Send our transformation to the currently bound shader, in the "MVP" uniform
		glUniformMatrix4fv(matrixID, 1, GL_FALSE, &MVP[0][0]);

		// Bind our texture in Texture Unit 0
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, textureID);
		// Set our "myTextureSampler" sampler to user Texture Unit 0
		glUniform1i(textureUniformID, 0);

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(
			0,                  // attribute
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);

		// 2nd attribute buffer : UVs
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
		glVertexAttribPointer(
			1,                                // attribute
			2,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
		);

		// Index buffer
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

		// Draw the triangles !
		glDrawElements(
			GL_TRIANGLES,      // mode
			indices.size(),    // count
			GL_UNSIGNED_SHORT,   // type
			(void*)0           // element array buffer offset
		);

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	} while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && !glfwWindowShouldClose(window)); // Check if the ESC key was pressed or the window was closed

	// Cleanup VBO and shader
	glDeleteBuffers(1, &vertexbuffer);
	glDeleteBuffers(1, &uvbuffer);
	glDeleteBuffers(1, &elementbuffer);
	glDeleteProgram(programID);
	glDeleteTextures(1, &textureID);
	glDeleteVertexArrays(1, &vertexArrayID);

	glfwTerminate();

	return 0;
}