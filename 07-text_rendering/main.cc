/**
 * Based on:
 * - http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-11-2d-text/
 * - http://www.opengl-tutorial.org/miscellaneous/an-fps-counter/
 */

#include <stdio.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "util.h"
#include "trackball_camera.h"
#include "text_renderer.h"

GLFWwindow* window;

glm::mat4 projection;

glab::TrackballCamera _camera(glm::vec3(0, 0, 0), 5.0f);
const float cameraRotationSpeed = 0.005f;
double mouseX, mouseY;

glab::TextRenderer* textRenderer = NULL;

void mouseMoved(GLFWwindow* window, double xpos, double ypos) {
	const float xDiff = float(xpos - mouseX);
	const float yDiff = float(ypos - mouseY);
	mouseX = xpos;
	mouseY = ypos;

	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
		_camera.pitch(yDiff * -cameraRotationSpeed);
		_camera.yaw(xDiff * -cameraRotationSpeed);
	}
}

void mouseScroll(GLFWwindow* window, double xoffset, double yoffset) {
	_camera.setRadius(_camera.getRadius() + float(yoffset) * cameraRotationSpeed * -50);
}

void mousePressed(GLFWwindow* window, int button, int action, int mods) {
	if (action == GLFW_PRESS) {
		glfwGetCursorPos(window, &mouseX, &mouseY);
	}
}

void windowResized(GLFWwindow* window, int width, int height) {
	const float ratio = width / float(height);

	projection = glm::perspective(glm::radians(45.0f), ratio, 0.1f, 100.0f);

	glViewport(0, 0, width, height);
}

void drawTimmingStuff() {
	static double lastTime = glfwGetTime(); // glfwGetTime is called only once, the first time this function is called
	static unsigned framesCount = 0u;
	static char frameTimeStr[256] = "ms/frame = 0";
	static char fpsStr[256] = "FPS = 0";

	++framesCount;

	if (glfwGetTime() - lastTime >= 1.0) {
		const double frameTime = 1000.0 / double(framesCount);

		sprintf(frameTimeStr, "ms/frame = %.2f", frameTime);
		sprintf(fpsStr, "FPS = %.2f", 1000.0 / frameTime);

		framesCount = 0u;
		lastTime += 1.0;
	}

	textRenderer->printText2D(frameTimeStr, -0.9f, -0.9f, 0.05f);
	textRenderer->printText2D(fpsStr, -0.9f, 0.9f, 0.05f);
}

int main() {
	if (!glfwInit()) {
		fprintf(stderr, "Failed to initialize GLFW\n");
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(1024, 768, "GLAB - Text Rendering", NULL, NULL);

	if (window == NULL) {
		fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not OpenGL 3.3 compatible.\n");
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	glfwSwapInterval(0); // disable vsync

	glfwSetCursorPosCallback(window, mouseMoved);
	glfwSetMouseButtonCallback(window, mousePressed);
	glfwSetScrollCallback(window, mouseScroll);
	glfwSetFramebufferSizeCallback(window, windowResized);
	
	// init GLEW
	glewExperimental = true; // http://glew.sourceforge.net/basic.html

	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE); // Ensure we can capture the escape key being pressed below
	
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f); // Dark blue background

	std::string shadersDir;

	if (!glab::util::findDirectory("07-text_rendering", shadersDir)) {
		fprintf(stderr, "Unable to find shaders directory!\n");
		return -1;
	}

	GLuint programID = glab::util::loadShadersFromFiles(shadersDir + "shader.vert", shadersDir + "shader.frag");

	if (!programID) {
		fprintf(stderr, "Failed to load shaders!\n");
		return -1;
	}

	// Get a handle for our "MVP" uniform
	GLuint matrixID = glGetUniformLocation(programID, "MVP");

	// Projection matrix : 45� Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	projection = glm::perspective(glm::radians(45.0f), 4.0f / 3.0f, 0.1f, 100.0f);
	// Or, for an ortho camera :
	//projection = glm::ortho(-10.0f,10.0f,-10.0f,10.0f,0.0f,100.0f); // In world coordinates

	GLuint vertexArrayID;
	glGenVertexArrays(1, &vertexArrayID);
	glBindVertexArray(vertexArrayID);

	// An array of 3 vectors which represents 3 vertices
	static const GLfloat g_vertex_buffer_data[] = {
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
	};

	GLuint vertexbuffer; // This will identify our vertex buffer
	glGenBuffers(1, &vertexbuffer); // Generate 1 buffer, put the resulting identifier in vertexbuffer
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW); // Give our vertices to OpenGL

	textRenderer = new glab::TextRenderer(shadersDir + "Holstein.DDS");

	do {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		const glm::mat4 view = _camera.getViewMatrix();

		// Model matrix : an identity matrix (model will be at the origin)
		const glm::mat4 model = glm::mat4(1.0f);

		// Our ModelViewProjection : multiplication of our 3 matrices
		const glm::mat4 MVP = projection * view * model; // Remember, matrix multiplication is the other way around

		glUseProgram(programID);

		// Send our transformation to the currently bound shader, in the "MVP" uniform
		glUniformMatrix4fv(matrixID, 1, GL_FALSE, &MVP[0][0]);

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(
			0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);

		// Draw the triangle !
		glDrawArrays(GL_TRIANGLES, 0, 3); // Starting from vertex 0; 3 vertices total -> 1 triangle

		glDisableVertexAttribArray(0);

		drawTimmingStuff();

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	} while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && !glfwWindowShouldClose(window)); // Check if the ESC key was pressed or the window was closed
	
	delete textRenderer;
	textRenderer = NULL;

	// Cleanup VBO and shader
	glDeleteBuffers(1, &vertexbuffer);
	glDeleteProgram(programID);
	glDeleteVertexArrays(1, &vertexArrayID);

	glfwTerminate();

	return 0;
}

