/**
* Based on http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-14-render-to-texture/
*/

#include <stdio.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "util.h"
#include "trackball_camera.h"

/**
 *                (2)________________(6)
 *                  |\               |\
 *                  | \              | \
 *                  |  \             |  \
 *                  |(3)\________________\(7) = max vertex
 *                  |   |            |   |
 *  min vertex = (0)|_ _|_ _ _ _ _ _ |(4)|
 *                   \  |             \  |
 *                    \ |              \ |
 *                     \|_______________\|
 *                     (1)               (5)
 *     |y+
 *     |
 *     |_____x+
 *      \
 *       \z+
 */
namespace {
	static const GLfloat g_vertex_buffer_data[] = {
		-0.5f, -0.5f, -0.5f, // 0
		-0.5f, -0.5f,  0.5f, // 1
		-0.5f,  0.5f, -0.5f, // 2
		-0.5f,  0.5f,  0.5f, // 3
		 0.5f, -0.5f, -0.5f, // 4
		 0.5f, -0.5f,  0.5f, // 5
		 0.5f,  0.5f, -0.5f, // 6
		 0.5f,  0.5f,  0.5f  // 7
	};

	static const GLfloat g_color_buffer_data[] = {
		0.0f, 0.0f, 0.0f, // 0
		0.0f, 0.0f, 1.0f, // 1
		0.0f, 1.0f, 0.0f, // 2
		0.0f, 1.0f, 1.0f, // 3
		1.0f, 0.0f, 0.0f, // 4
		1.0f, 0.0f, 1.0f, // 5
		1.0f, 1.0f, 0.0f, // 6
		1.0f, 1.0f, 1.0f  // 7
	};

	static const unsigned short g_index_buffer_data[] = {
		0, 1, 3,
		0, 3, 2,
		1, 5, 7,
		1, 7, 3,
		5, 4, 6,
		5, 6, 7,
		4, 0, 6,
		0, 2, 6,
		3, 7, 6,
		3, 6, 2,
		0, 4, 5,
		0, 5, 1
	};
}

GLFWwindow* window;

glm::mat4 projection;

glab::TrackballCamera _camera(glm::vec3(0, 0, 0), 5.0f);
const float cameraRotationSpeed = 0.005f;
double mouseX, mouseY;

int screenWidth = 1024, screenHeight = 768;

void mouseMoved(GLFWwindow* window, double xpos, double ypos) {
	const float xDiff = float(xpos - mouseX);
	const float yDiff = float(ypos - mouseY);
	mouseX = xpos;
	mouseY = ypos;

	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
		_camera.pitch(yDiff * -cameraRotationSpeed);
		_camera.yaw(xDiff * -cameraRotationSpeed);
	}
}

void mouseScroll(GLFWwindow* window, double xoffset, double yoffset) {
	_camera.setRadius(_camera.getRadius() + float(yoffset) * cameraRotationSpeed * -50);
}

void mousePressed(GLFWwindow* window, int button, int action, int mods) {
	if (action == GLFW_PRESS) {
		glfwGetCursorPos(window, &mouseX, &mouseY);
	}
}

GLuint renderedTexture;
GLuint depthrenderbuffer;

void windowResized(GLFWwindow* window, int width, int height) {
	screenWidth = width;
	screenHeight = height;

	const float ratio = width / float(height);

	projection = glm::perspective(glm::radians(45.0f), ratio, 0.1f, 100.0f);

	// resize rendered texture
	glBindTexture(GL_TEXTURE_2D, renderedTexture); // "Bind" the newly created texture : all future texture functions will modify this texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, screenWidth, screenHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, 0); // Give an empty image to OpenGL ( the last "0" means "empty" )

	// resize depth buffer
	glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, screenWidth, screenHeight);
}

int main() {
	if (!glfwInit()) {
		fprintf(stderr, "Failed to initialize GLFW\n");
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(screenWidth, screenHeight, "GLAB - Render to Texture (RTT)", NULL, NULL);

	if (window == NULL) {
		fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not OpenGL 3.3 compatible.\n");
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	glfwSetCursorPosCallback(window, mouseMoved);
	glfwSetMouseButtonCallback(window, mousePressed);
	glfwSetScrollCallback(window, mouseScroll);
	glfwSetFramebufferSizeCallback(window, windowResized);

	// init GLEW
	glewExperimental = true; // http://glew.sourceforge.net/basic.html

	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE); // Ensure we can capture the escape key being pressed below

	glClearColor(0.0f, 0.0f, 0.4f, 0.0f); // Dark blue background

	glEnable(GL_DEPTH_TEST); // Enable depth test	
	glDepthFunc(GL_LESS); // Accept fragment if it closer to the camera than the former one

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	std::string shadersDir;

	if (!glab::util::findDirectory("08-rtt", shadersDir)) {
		fprintf(stderr, "Unable to find shaders directory!\n");
		return -1;
	}

	GLuint programID = glab::util::loadShadersFromFiles(shadersDir + "shader.vert", shadersDir + "shader.frag");

	if (!programID) {
		fprintf(stderr, "Failed to load shaders!\n");
		return -1;
	}

	// Get a handle for our "MVP" uniform
	GLuint matrixID = glGetUniformLocation(programID, "MVP");

	// Projection matrix : 45� Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	projection = glm::perspective(glm::radians(45.0f), 4.0f / 3.0f, 0.1f, 100.0f);
	// Or, for an ortho camera :
	//projection = glm::ortho(-10.0f,10.0f,-10.0f,10.0f,0.0f,100.0f); // In world coordinates

	GLuint vertexArrayID;
	glGenVertexArrays(1, &vertexArrayID);
	glBindVertexArray(vertexArrayID);

	GLuint vertexbuffer; // This will identify our vertex buffer
	glGenBuffers(1, &vertexbuffer); // Generate 1 buffer, put the resulting identifier in vertexbuffer
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW); // Give our vertices to OpenGL

	GLuint colorbuffer;
	glGenBuffers(1, &colorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_color_buffer_data), g_color_buffer_data, GL_STATIC_DRAW);

	GLuint elementbuffer;
	glGenBuffers(1, &elementbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(g_index_buffer_data), g_index_buffer_data, GL_STATIC_DRAW);

	// RTT stuff:

	// The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
	GLuint framebufferName = 0;
	glGenFramebuffers(1, &framebufferName);
	glBindFramebuffer(GL_FRAMEBUFFER, framebufferName);

	// The texture we're going to render to
	glGenTextures(1, &renderedTexture);
	glBindTexture(GL_TEXTURE_2D, renderedTexture); // "Bind" the newly created texture : all future texture functions will modify this texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, screenWidth, screenHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, 0); // Give an empty image to OpenGL ( the last "0" means "empty" )
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); // Poor filtering
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	// The depth buffer
	glGenRenderbuffers(1, &depthrenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, screenWidth, screenHeight);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);

	//// Alternative : Depth texture. Slower, but you can sample it later in your shader
	//GLuint depthTexture;
	//glGenTextures(1, &depthTexture);
	//glBindTexture(GL_TEXTURE_2D, depthTexture);
	//glTexImage2D(GL_TEXTURE_2D, 0,GL_DEPTH_COMPONENT24, 1024, 768, 0,GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); 
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	// Set "renderedTexture" as our colour attachement #0
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderedTexture, 0);

	//// Depth texture alternative : 
	//glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture, 0);
	
	// Set the list of draw buffers.
	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, DrawBuffers); // "1" is the size of DrawBuffers

	// Always check that our framebuffer is ok
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		return false;

	////

	// The fullscreen quad's FBO
	static const GLfloat g_quad_vertex_buffer_data[] = {
		-1.0f, -1.0f,
		1.0f, -1.0f,
		-1.0f, 1.0f,
		-1.0f, 1.0f,
		1.0f, -1.0f,
		1.0f, 1.0f
	};

	GLuint quad_vertexbuffer;
	glGenBuffers(1, &quad_vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, quad_vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_quad_vertex_buffer_data), g_quad_vertex_buffer_data, GL_STATIC_DRAW);

	// Create and compile our GLSL program from the shaders
	GLuint quad_programID = glab::util::loadShadersFromFiles(shadersDir + "passthrough.vert", shadersDir + "wobblyTexture.frag");
	GLuint texID = glGetUniformLocation(quad_programID, "renderedTexture");
	GLuint timeID = glGetUniformLocation(quad_programID, "time");

	do {
		// Render to our framebuffer
		glBindFramebuffer(GL_FRAMEBUFFER, framebufferName);
		glViewport(0, 0, screenWidth, screenHeight); // Render on the whole framebuffer, complete from the lower left corner to the upper right

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		const glm::mat4 view = _camera.getViewMatrix();

		// Model matrix : an identity matrix (model will be at the origin)
		const glm::mat4 model = glm::mat4(1.0f);

		// Our ModelViewProjection : multiplication of our 3 matrices
		const glm::mat4 MVP = projection * view * model; // Remember, matrix multiplication is the other way around

		glUseProgram(programID);

		// Send our transformation to the currently bound shader, in the "MVP" uniform
		glUniformMatrix4fv(matrixID, 1, GL_FALSE, &MVP[0][0]);

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(
			0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);

		// 2nd attribute buffer : colors
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
		glVertexAttribPointer(
			1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
		);

		// Index buffer
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

		// Draw the triangle !
		glDrawElements(
			GL_TRIANGLES,      // mode
			sizeof(g_index_buffer_data)/sizeof(unsigned short),    // count
			GL_UNSIGNED_SHORT,   // type
			(void*)0           // element array buffer offset
		);

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);

		//////////////////////////////////
		
		// Render to the screen
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		//glViewport(0, 0, screenWidth, screenHeight); // Render on the whole framebuffer, complete from the lower left corner to the upper right

		// Clear the screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Use our shader
		glUseProgram(quad_programID);

		// Bind our texture in Texture Unit 0
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, renderedTexture);
		// Set our "renderedTexture" sampler to user Texture Unit 0
		glUniform1i(texID, 0);

		glUniform1f(timeID, (float)(glfwGetTime()*10.0f));

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, quad_vertexbuffer);
		glVertexAttribPointer(
			0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
			2,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);

		// Draw the triangles !
		glDrawArrays(GL_TRIANGLES, 0, 6); // 2*3 indices starting at 0 -> 2 triangles

		glDisableVertexAttribArray(0);

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	} while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && !glfwWindowShouldClose(window)); // Check if the ESC key was pressed or the window was closed

	// Cleanup VBO and shader
	glDeleteBuffers(1, &vertexbuffer);
	glDeleteBuffers(1, &colorbuffer);
	glDeleteBuffers(1, &elementbuffer);
	glDeleteProgram(programID);
	glDeleteVertexArrays(1, &vertexArrayID);

	glDeleteFramebuffers(1, &framebufferName);
	glDeleteTextures(1, &renderedTexture);
	glDeleteRenderbuffers(1, &depthrenderbuffer);
	glDeleteBuffers(1, &quad_vertexbuffer);

	glfwTerminate();

	return 0;
}

