MACRO(addThirdParty thirdpartydir)
	message("Third party directory: " ${thirdpartydir})

	list(APPEND CMAKE_MODULE_PATH "${thirdpartydir}/ThirdParty.cmake/CMakeModules")
	include(thirdparty)
	include(source_group_tree)
	
	if(WIN32)
		if(MSVC)
			file(GLOB built_thirdparty ${thirdpartydir}/built/msvc )		
		elseif(MINGW)
			file(GLOB built_thirdparty ${thirdpartydir}/built/mingw )	
		else()
			message(FATAL_ERROR "Compiler is neither MSVC nor MINGW")
		endif()
		
		add_definitions(-DBOOST_ALL_NO_LIB) # disable boost autolink
		
		THIRDPARTY_DIR(${built_thirdparty})
	endif()
	
	# Place CMakeLists.txt file for GLEW
	file(COPY ${thirdpartydir}/../CMakeLists.txt.glew DESTINATION ${thirdpartydir}/glew/)
	file(RENAME ${thirdpartydir}/glew/CMakeLists.txt.glew ${thirdpartydir}/glew/CMakeLists.txt)
	
	add_subdirectory(${thirdpartydir}/glew)
	add_subdirectory(${thirdpartydir}/glfw)
	add_subdirectory(${thirdpartydir}/glm)
	
	# MinGW cannot compile ASSIMP IFC importer. Issue described at:
	# https://github.com/assimp/assimp/issues/177
	add_definitions(-DASSIMP_BUILD_NO_IFC_IMPORTER)
	
	option(ASSIMP_BUILD_TESTS OFF)
	option(ASSIMP_BUILD_ASSIMP_TOOLS OFF)
	
	# The following modifications were made in ${thirdpartydir}/assimp/CMakeLists.txt:
	# 1) All CMAKE_SOURCE_DIR were replaced by CMAKE_CURRENT_SOURCE_DIR
	# 2) All CMAKE_BINARY_DIR were replaced by CMAKE_CURRENT_BINARY_DIR
	add_subdirectory(${thirdpartydir}/assimp)
ENDMACRO()