#version 330 core

// Interpolated values from the vertex shaders
in vec2 UV;
in vec3 vertexPosition_worldspace; // fragment position (interpolated from vertex position)
in vec3 vertexNormal_cameraspace;
in vec3 eyeDirection_cameraspace; // direction from fragment to camera
in vec3 lightDirection_cameraspace; // direction from fragment to light

// Ouput data
out vec4 color;

// Values that stay constant for the whole mesh.
uniform sampler2D myTextureSampler;
uniform vec3 lightPosition_worldspace;

// Light emission properties. You probably want to put them as uniforms
const vec3 lightColor = vec3(1, 1, 1);
const float lightPower = 50.0f;

void main() {
	// Material properties
	vec3 materialDiffuseColor = texture2D(myTextureSampler, UV).rgb;
	vec3 materialAmbientColor = vec3(0.1, 0.1, 0.1) * materialDiffuseColor;
	vec3 materialSpecularColor = vec3(0.6, 0.6, 0.6);

	// Distance from fragment to light
	float lightDistance = length(lightPosition_worldspace - vertexPosition_worldspace);
	float lightDistance2 = lightDistance * lightDistance;

	vec3 N = normalize(vertexNormal_cameraspace); // Fragment normal (interpolated from vertex normal)
	vec3 L = normalize(lightDirection_cameraspace); // Direction from the fragment to the light

	// Cosine of the angle between the normal and the light direction, clamped above 0
	float cosTheta = clamp(dot(N, L), 0, 1);

	vec3 E = normalize(eyeDirection_cameraspace); // Direction from fragment to camera
	vec3 R = reflect(-L, N); // Reflects direction from light to fragment for fragment normal // R = (-L) - 2.0 * dot(N, (-L)) * N.

	// Cosine of the angle between the Eye vector and the Reflect vector, clamped to 0
	float cosAlpha = clamp(dot(E, R), 0, 1);

    // Output color = color of the texture at the specified UV
    vec3 fragColor = materialAmbientColor;
	fragColor += materialDiffuseColor * lightColor * lightPower * cosTheta / lightDistance2;
	fragColor += materialSpecularColor * lightColor * lightPower * pow(cosAlpha, 5) / lightDistance2;

	color = vec4(fragColor, 1.0);
}